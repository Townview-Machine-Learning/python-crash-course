##### INTRODUCTION #####

# In this lesson, we will build on the topics we learned about with our previous lessons
# We will also be learning completely new topics such as subplots





##### SETUP #####

# Once again, our first step will be to import the matplotlib library
# We have already shown the steps for how to install it in the previous lesson
# So all we need to do now is to import it

import matplotlib.pyplot as plt
import numpy as np

# Once again, if you are using Jupyter Notebooks, 
# you need to use this command to ensure the graphs will be plotted within the same code block

# %matplotlib inline

##### BASIC MULTIPLOTS #####

# Now that we have successfully imported these libraries, we can set up the figure objects we're going to be using
# If you would like a refresher on such concepts, our Basic Plotting lesson is also included in this folder

f = plt.figure()

# We just created our figure object, and now we will create our axes

a1 = f.add_axes([0, 0, 1, 1])
a2 = f.add_axes([0.2, 0.5, 0.2, 0.2])

# Notice how we created two axes, as opposed to the usual one
# This is called a multiplot, and it describes when multiple axes of an object are in the same figure object
# Before finishing the creation of our multiplots, we need to create the data that our plots will use
# For this example, we'll be using numpy arrays, which is why we imported that package above
# Now we can define our numpy arrays

x = np.linspace(0, 5, 11)
y = x**3

# As you can see, we have created two numpy arrays
# The first contains 11 values evenly spaced that are found between 0 and 5
# The second simply contains all the values of the first array cubed
# These numpy arrays, like that of the past lesson, will be kept simple
# so that the main focus of the lesson can be placed on how to use multiplots and subplots

# In later examples, you can apply your knowledge of pandas (refer to previous lessons on pandas)
# to incorporate real life data in these examples

# Now, we can finally define our axes

a1.plot(x, y)
a2.plot(y, x)
plt.show()

# While this is a simple example, it shows what is possible using multiplots,
# and how a large amount of information can easily be conveyed in a simple manner





##### SUBPLOTS #####

# If there was a simple to describe subplots, it would be that they are automatic axis managers
# Let's go over an example of how they work

# First, we have to define our figure and our axes

f, a = plt.subplots()

# This may look similar to how we defined our figure() object, 
# but notice how the subplots() object needs a tuple of variables: one for the figure and the other for the axes

# Now, you can define your axes just like normal

a.plot(x, y)
plt.show()

# Now, let's create a figure with multiple subplots

f, a = plt.subplots(nrows=1, ncols=2)

# As you can see, these rows and columns display how many of these subplots there will be on each axis

# Now, instead of having to manually define each of these subplots, we can easily just iterate through them with a for loop

for subplot in a:
    subplot.plot(x, y)
plt.show()

# As you can see, we iterated through our axes and plotted x and y on all of them
# While this may not be too useful for only 2 subplots, it can be handy when working with larger amounts of subplots

# A common issue with subplots is usually the overlapping of plots
# The bright side, however, is that there's an easy fix to this

f.tight_layout()

# This method call fixes the layout of all the axes in an organized manner

# While this lesson is a bit shorter than the others, feel free to try the concepts you have learned here 
# alongside some of the added customization in the previous lesson regarding colors, patterns, titles, etc.
