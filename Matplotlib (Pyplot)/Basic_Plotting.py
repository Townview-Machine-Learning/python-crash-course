##### INTRODUCTION #####

# In this series of lessons, we will be learning about a python library called Matplotlib
# In our previous lessons, we learned about the basics of python and how to organize and analyze data
# Using Matplotlib, we can visualize this data in a simple and efficient manner
# In this lesson, we'll go over how to install Matplotlib as well as how to start basic plotting





##### INSTALLATION AND SETUP #####

# Our first step is installing the Matplotlib library
# We can do this by using the "pip install matplotlib" command in our terminal

# Now that we have done that, we can import the library

import matplotlib.pyplot as plt

# If you are using jupyter notebooks, you also have to use this command to see plots in the notebook:

%matplotlib inline





##### BASIC PLOTTING #####

# Let's create a basic numpy array to use as an example
# Before we do that however, we need to import the package

import numpy as np

x = np.linspace(0, 5, 11)
y = x**2

# As you can see, we have created two numpy arrays
# The first array containts 11 evenly spaced values between 0 and 5
# The second array is just the first one with each value squared

# Now, let's go over a basic matplotlib command:

plt.plot(x, y, 'r')
plt.show()

# As you can see, we have plotted a graph of our two numpy arrays
# Let's go over what we just did with this method call
# We used the .plot() function and used 3 parameters: an array that represents the x values,
# an array that represents the y values, and finally, the color of the line
# Of course, the last part isn't necessary, but it's useful if you have a lot of graphs later on

# You can also change the title of each of the axes, as well as the title of the entire plot

plt.plot(x, y, 'r')
plt.xlabel('random x axis')
plt.ylabel('random y axis')
plt.title('random title :D')
plt.show()

# As you can see, we have the same graph that we created before, but this time with titles. Wow!





##### OBJECT ORIENTED PLOTTING AND FIGURES #####





# Now that we've covered basic plotting techniques, we can discover the more object oriented aspect of matplotlib.
# The first thing we're going to do is define and instantiate an empty figure.

f = plt.figure()

# Now that we've created out figure, we can change it to fit our needs.
# The first step in doing that is adding scales for our axes

a = f.add_axes([0, 0, 1, 1])

# Notice how we defined a new variable for this specific graph within this figure
# The main parameter for this method is an array, which consists of four values
# The first two define where the graph is found on the canvas, which isn't too important
# The second two values define the height and width of the graph
# All four of these values need to be in a range of 0 to 1

# Now, let's add the data and the titles for the graph

a.plot(x, y)
a.set_xlabel('random x axis')
a.set_ylabel('random y axis')
a.set_title('random title')
plt.show()

# To show how flexible such figures are, let's go over another example
# Let's start by creating a blank figure and creating two axes

a1 = f.add_axes([0.1, 0.1, 0.8, 0.8])
a2 = f.add_axes([0.2, 0.5, 0.4, 0.3])

a1.plot(x, y)
a2.plot(y, x)

plt.show()

# As you can see, we have just created a simple plot within a plot in just a few lines of code

# The final bit of information that is good to know is how to save plots, and it can easily be done through a simple method call

f.savefig('exampleplot.png')

# You can choose to specify the DPI as well as the various output formats, but those are generally not too important





##### LEGENDS #####





# While we have already learned how to use labels and titles in the above sections, we have yet to learn how to add legends to figures
# The first thing we need to do is create a new blank figure to work with

f = plt.figure()

a = f.add_axes([0, 0, 1, 1])

a.plot(x, x**2, label='x^2')
a.plot(x, x**3, label='x^3')
a.plot(x, x**4, label='x^4')

# Now that we have all that sorted, all we need to do is call the legend() method and we will have a legend for our plot

a.legend()

# We can choose to put or legends in different locations by inputting a value in the method for which area we want the legend to be in

a.legend(loc=1)

# There are many options for which location the legend can be, so it is recommended to look at the matplotlib documentation for specific details
# However, the most commonly used option for loc is loc=0, because that means matplotlip itself will decide the optimal location for the legend, making your life much easier.

a.legend(loc=0)





##### CUSTOMIZATION #####





# We have reached the most important portion of this lesson: the colors!
# Matplotlib has an absurd amount of options for how you can customize your graph, and while this will be more of a high level overview of that, more detailed information can be found on the Matplotlib documentation

# The first, and probably the most simple portion of this are the colors
# When plotting an axis, one of the optional parameters is color, and it is pretty self explainatory for the most part

a.plot(x, x+1, color = 'blue')

# You can also use RGB hex codes for this color value

a.plot(x, x+1, color = '#8B008B')

# Alongside color, you can also change the linewidth and transparency of the line in the same manner

a.plot(x, x+1, color = 'blue', alpha = 0.5, linewidth = 2)

# If you wish to create an even more elaborate line for some reason, you can do that as well with all the styles and markers Matplotlib offers

a.plot(x, x+1, color = 'blue', ls='-', marker='o')

# As you can see, the depth of customization in Matplotlib is absurd. If you would like a more in depth explaination of such mechanics, the Matplotlib documentation will most likely be the best source
# For the purposes of this lesson, simple customization of a line is sufficient
