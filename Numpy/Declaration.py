##### INTRODUCTION #####

# In this lesson, you will be learning about one of the most important libraries for machine learning and data science: numpy
# We will teach you how to install and import numpy, as well as how to declare a numpy array.





##### INSTALLATION  AND SETUP #####

# The first step we have to take is installing numpy.

# We can do this by typing 'pip install numpy' or 'conda install numpy' in the terminal (depending on your version of python)
# Now that we have done this, we can import numpy into our file

import numpy as np





##### INITIALIZATION OF NUMPY ARRAYS ######

# Next, let's see how we can create a numpy array
# First, let's create a normal Python list

normal_list = [1, 2, 3, 4, 5]

# Now, with just this list, we are able to create a new numpy array

fancy_list = np.array(normal_list)

# However, arrays do not need to be only 1-dimensional
# We can create something called a 2-dimensional array, where each value is represented by two coordinates

normal_array = [[1, 2, 3],[4, 5, 6],[7, 8, 9]]

# Now, if we convert that to a numpy array, we have created our first 2-dimensional matrix!

fancy_array = np.array(normal_array)





##### BASIC NUMPY METHODS #####

# Now, let's learn a few basic, built-in methods

# The first one we should learn is np.arange()
# This method returns an array of evenly-spaced values within a given interval
# If I wanted an array that ranged from 0-9, I would just write:

one_to_ten = np.arange(0, 10)

# I can also set the increment of this operation.
# So if I wanted an array that ranged from 0-10 and jumped in increments of 2, I would write:

evens = np.arange(0, 11, 2)

# Another simple (but useful) method is np.zeros and np.ones, which generates an array of only zeros and ones

only_zeros = np.zeros(3)

# If you wanted a multiple dimension array for this function, you can just use a tuple as your parameter, and put the dimensions of the matrix you want

only_ones = np.ones((5, 5))

# Another useful methed is np.linspace, which provides an array of evenly spaced numbers over a specific interval

evenly_spaced = np.linspace(0, 10, 50)