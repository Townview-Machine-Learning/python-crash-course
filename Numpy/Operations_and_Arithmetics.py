##### INTRODUCTION #####

# Welcome to the last lesson on numpy! 
# Now that we have covered declarations, indexing, and various attributes/methods, 
# we can focus on the final portion of the basic numpy course: arithmetic operations
# Let's get into it!





##### BASIC ARITHMETIC OPERATIONS #####

# Before we start our examples let's import numpy and create an arrays we can use.

import numpy as np

arr = np.arange(1,10)
print(arr)

# Let's go over a simple operation: adding two arrays together.

added_arr = arr + arr
print(added_arr)

# As you can see, each of the indeces of the array were added to each other.
# You can probable already guess that subtraction works in a similar manner to this.

subbed_arr = arr - arr
print(subbed_arr)

# The new array is full of zeros because you are subtracting two instances of the same array, meaning that the difference of all their values is, you guessed it, zero!
# Now, let's try out multiplication.
# Again, this works in a similar manner to addition and subtraction

multi_arr = arr * arr
print(multi_arr)

# However, once you get to division, you start to see some interesting things happen.
# Let's continue with the types of examples we have done above, except for division.

divid_arr = arr / arr
print(divid_arr)

# This may look normal to you, but notice the value at the first index (index 0) of the array.
# Instead of giving a numerical value, it instead has the value of something called NaN.
# Well, if you think back to the original values of the array, dividing the first values would result in a 0/0 equation.
# NaN is python's way of saying a value is undefined, much like how Java uses null.
# Also notice how this operation does not give an error to the user. It gives an alert, but the program is still allowed to run.

# There's another exceptional output that you can get from dividing, but let's first go into another basic aspect of basic numpy operations.
# Obviously, you aren't just going to be adding numpy arrays to each other.
# Because of this, you can also add, subtract, multiply, and divide numbers from numpy arrays.
# Let's go over a few examples of this.

print(arr)
added_arr = arr + 1
print(added_arr)

# As you can see, adding one does about what you would expect: add 1 to each value.
# We can skip the examples for subtraction and multiplication, but there is something interesting going on with the division example, so let's go over that.

print(arr)
divid_arr = 1/arr
print(divid_arr)

# Once again, it's the first value that we ned to pay attention to.
# At that index, the value of 1/0 is being shown, and as you can see, it is being displayed as inf.
# This signifies that the value diverges to positive infinity.
# Saying that, yes, it is possible to have a negative value for inf as well.

# Now that we got that out of the way, we can quickly go over exponents (and in turn, radicals) as well.

exp_arr = arr ** arr
print(exp_arr)

# Unsurprisingly, exponents work the same way as all basic operations we just went over. 
# Here, for each index of the array, you are given the value of one thing to the power of the other. 
# For example: 0^0, 1^1, 2^2, 3^3, 4^4, etc.

# Obviously, you can do such a thing with just numbers as well.

exp_arr = arr ** 2;
print(exp_arr)

# Here, all the values in arr have been squared. 





##### UNIVERSAL FUNCTIONS #####

# Here, we're going to be covering something new.
# These are called universal functions, or ufuncs, and what they do is pretty interesting.
# In simple terms, these functions offer a new way of iterating over elements called vectorization, which is much faster and more optimized for modern computers.
# Aside from the performance boost, ufuncs also offer various different functionalities that aren't really given by the normal method of iteration.
# All right, with all that aside, let's go over how to make one.

# We'll start with something simple, just an addition ufunc.
# We'll start by defining a real function for adding two numpy arrays.

def addMine(x, y):
    return x+y

# Now, we can define our ufunc using np.frompyfunc()

add_ufunc = np.frompyfunc(addMine, 2, 1)

# Let's go over what we did real quick.
# The first parameter in this method call is the function we want to use for our ufunc.
# The next part is how many input arrays the ufunc will take, which is, in this case, 2 input arrays.
# Finally, the last parameter shows how many arrays the function will output, which is just one.
# Let's put this ufunc to the test.

print(add_ufunc([1,2,3,4,5], [6,7,8,9,10]))

# If you want to know whether a method is a ufunc or not, you can use the type() ufunc.

print(type(add_ufunc))

# We know this is a ufunc because this returns "<class 'numpy.ufunc'>"
# For another numpy method that happens to not be a ufunc, the type call will output "<class 'builtin_function_or_method'>"

# As we've gone over in the last section, you can use simple arithmetic symbols for operations with numpy arrays.
# However, it is also a possibility to use ufuncs for such a purpose.

added_arr = np.add(arr, arr)
print(added_arr)

subbed_arr = np.subtract(arr, arr)
print(subbed_arr)

multi_arr = np.multiply(arr, arr)
print(multi_arr)

divid_arr = np.divide(arr, arr)
print(divid_arr)

exp_arr = np.power(arr, arr)
print(exp_arr)

mod_arr = np.mod(arr, arr)
print(mod_arr)

# As you can see, these ufuncs are an alternative to what was shown in the basic operations and arithmetics section.

# Now, we can just go over a BUNCH of useful ufuncs for numpy arrays.

print(np.trunc([-3.543, 4.323]))

# This ufunc removes the decimals in a value. In fact, the fix ufunc does the same thing as well.

print(np.around([-3.543, 4.323],2))

# As you can see, the round ufunc's first parameter is the value(s) that are going to be rounded, and the second parameter is how many decimal points those values are being rounded to.

print(np.floor([-3.543, 4.323]))

# The floor ufunc is used to round each value to the lowest integer.

print(np.ceil([-3.543, 4.323]))

# As you can probably tell, ceil does the exact opposite of floor and rounds each value to the highest integer.


# ...and that's it for basic ufuncs!
