
##### INTRODUCTION #####

# The next topic we will be discussing is various methods and attributes of a numpy array.





##### USEFUL METHODS #####

# The first method we'll be learning is the .max() method.
# This method allows you to find the maximum value in a numpy array.
# Let's make a numpy array to use as an example.

example_array = np.random.randint(0,50,10)

print(example_array)

# This array (just for review) has ten values in the range of 0-50
# Now, we can find the maximum value of the array using the .max() method.

print(example_array.max())

# Similar to this method, there is the .min() method.
# If you haven't somehow guessed already, this method finds the minimum value of a numpy array.

print(example_array.min())

# If you wanted to find the indeces of these values, there's a method for that as well.
# The .argmin() and .argmax() methods are used to find the indeces of the minimum and maximum values of a numpy array respectively.

print(example_array.argmin())

print(example_array.argmax())

# This next attribute is easily one of the most important for numpy arrays.
# Shape defines, for lack of better terms, the shape of an array.
# You're able to define the dimensionality of an array through this method.
# Let's go over an example.

print(example_array.shape)

# This statement prints the shape of the example array we defined above. now, let's go over how to reshape this array.
# The way we can do this is by using the method .reshape()

print(example_array.reshape(1,10))

# This array is the same size as the original, but notice the extra brackets.
# Now let's reshape it to something different.

print(example_array.reshape(2,5))

# Here, we have reshaped this array into a different array that retains the same number of values as the original.
# The final useful method we'll go over is called dtype, which shows the type of a numpy array

print(example_array.dtype)

# Here, we will learn about two useful methods for numpy arrays.
# The difference between these two methods is very important to know, because they are easy to confuse.
# The two methods we'll be learning about are the .copy() and the .view() method
# The .copy() method does about what you expect, and it copies the contents of an array into another array.

copied_array = example_array.copy()
print(example_array)
print(copied_array)

# However, there is one important distinction to make.
# The copied array now has no relation to the original one
# Meaning that if you change a value in the original or new array, the change will not be applied to both arrays.
# Let's go over an example.

copied_array[0] = 21
print(example_array)
print(copied_array)

# Notice how the change does not carry over to the example array.
# Now let's go over the next method.
# This is the .view() method.

viewed_array = example_array.view()
print(example_array)
print(viewed_array)

# At first glance, it make look the same as the .copy() method.
# There is one important distinction to make though.
# When an array is made with the .copy() method, that array "owns" those values.
# This means that when a change is made to the original array or the new one, those changes will only affect that array, and not the other.
# This is not the case with the .view() method.
# Let's go over an example.

viewed_array[0] = 21
print(example_array)
print(copied_array)

# Notice how the change carries over from the viewed array.
# The same thing happens when you change the original array as well.

example_array[4] = 23
print(example_array)
print(copied_array)

# So, now that you have learned the difference between an array that "owns" its data and one that doesn't, how can you tell the difference?
# It is very impractical to change a value and see if it changes in both arrays every time you want to check.
# Luckily, there is a useful attribute for that as well.
# The .base attribute shows whether an array is independent or not.
# Let's go over a few examples.

print(copied_array.base)

# With an array that was copied, the base is "None"
# This means that the array is "self-owned"
# Let's look at an example where the array is dependent on our example_array

print(viewed_array.base)

# With an array that is viewed, the base is the contents of the original array, or in this case, the contents of example_array
