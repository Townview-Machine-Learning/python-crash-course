##### INTRODUCTION #####

# Now that you have learned how to declare and initialize numpy arrays, you can learn more built in methods.
# The methods you will be learning in this specific lesson will be about methods involving random numbers.





##### BASIC RANDOM METHODS #####

# The first method you can learn is an easy one
# This method creates an array of a given size and fills it with random numbers in the range [0, 1)

rand_array1 = np.random.rand(2)

# You can also apply this method to different dimensions of arrays

rand_array2 = np.random.rand(5,5)

# There is also another method that outputs a different kind of random numbers into the array:
# randn outputs samples from the "standard normal" distribution.
# This means that the array will always have a mean of 0 and a standard deviation of 1

randn_array1 = np.random.randn(2)

# Again, you can also apply this method to different dimensions of arrays

randn_array2 = np.random.randn(5,5)

# A random method that returns random integers from low (inclusive) to high (exclusive)

randint_num = np.random.randint(1,100)

# Here, the method outputs a random number in the range [1,100)
# This method can also do such a thing with an array

randint_array = np.random.randint(1,100,10)

# With this array, every value falls into the range [1,100)