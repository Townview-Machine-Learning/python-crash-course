##### INTRODUCTION #####

# In this lesson, we'll go over various methods to index and select portions of numpy arrays.





##### INDEXING #####

# Array indexing is very simple in numpy, and it works very similar to regular lists in python and arrays in java.
# Let's define the numpy array we'll be using for our examples.

import numpy as np

arr = np.random.randint(0,50,10)
print(arr)

# Array indexing is done by using brackets and putting the desired index within the brackets.

print(arr[3])

# You can also use this strategy for multiple dimension arrays.

arr.reshape(2,5)
print(arr)

# This can be done by putting multiple indeces within the brackets for each of the dimensions of the array.

print(arr[0,1])

# Using multiple sets of brackets, such as arr[0][1], has the same effect.

# This technique works with any dimension of arrays.
# Much like normal arrays, you can also use negative indexing, which counts from the end of the array.
# This is only possible in Python, and if you try this with arrays in Java, you will get an error.

print(arr[1,-1])

# Now, let's go over a useful technique to get multiple values from an array as opposed to just one.
# The name of this is called slicing, and here's an example on how to use it.

arr = np.random.randint(0,50,25)
sliced_array = arr[1:17]
print(sliced_array)

# As you can see, the sliced array returns only the portion of the array that is made up of indeces 1 through 5.
# Just like normal indexing, you can also slice multiple dimenstion arrays.
# Let's go over an example of this.

arr.reshape(5,5)
print(arr)
sliced_array = arr[1:3, 3:4]
print(sliced_array)

# With slixing arrays, there is a useful technique to slice from the beginning of the array up to some point.

sliced_array1 = arr[0:3, 0:4]
print(sliced_array1)
sliced_array2 = arr[:3, :4]
print(sliced_array2)

# As you can see here, you can omit the zero and by default, the array will select from the beginning.
# Unsurprisingly, this can also be done with the end of the array as well, in the same fashion as well.

sliced_array1 = arr[1:4, 2:4]
print(sliced_array1)
sliced_array2 = arr[1:, 2:]
print(sliced_array2)

# Now, we're going to go over "fancy indexing."
# Obviously, this isn't an official name, but it fits pretty well.
# Let's go over the example by making a new array.

arr = np.zeros(10,10)
print(arr)

# Just to review, this statement creates a 10x10 matrix of just zeros

for i in range(10):
    arr[i] = i
print(arr)

# This next statement moves through all the rows of the array and turns their values into that of one (refer back to previous lessons for a more in depth explaination of for loops)
# Now that we have all that out of the way, we can focus on the "fancy indexing."
# Let's start with an example.

print(arr[[2,4,6,8]])

# As you can see, this statement prints the second, fourth, sixth, and eight rows of the array have been selected.
# This is not necessarily dependant on the order of the indeces given either, as shown in this next example.

print(arr[6,4,2,8])





##### SELECTION #####

# Here's the fun part. You have learned how to index and output specific values from arrays, and now you can select and filter values from such arrays.
# Let's make a new array to use for our examples.

arr = np.random.randint(0,50,25)
print(arr)

# Let's go over a simple example, where we return a boolen for every index where the condition is true or false.

print(arr > 25)

# As you can see, the indeces of this boolean array correspond with the original array, and every index with a value above 25 is counted as true, while every other number is counted as false.
# If you want, you can also assign this to a variable.

bool_arr = arr > 25
print(bool_arr)

# Now, let's take this one step further.
# We can use this boolean array as a condition for a new array so that only the values at the indeces where the boolean shows true are returned.
# Let's do an example.

new_arr = arr[bool_arr]
print(arr)
print(new_arr)

# You can also use the array itself as a parameter for such filters.

new_arr = arr[arr > 30]
print(new_arr)
