##### INTRODUCTION #####

# Pandas is a library that allows us to parse through data in formats like spreadsheets.
# This is helpful for machine learning to load and process data for training.

# First, you must install pandas. To do this, you can run the following command:
# pip install pandas

# Let's import pandas. "pd" is the alias commonly used for pandas.
import pandas as pd





##### SERIES #####

# Series are like one-dimensional arrays that can hold any data type.
# They are very similar to the ndarrays that we learned about in the numpy sections.

# We can make series from scalar values as well as iterable values.
# For example:
print(pd.Series(5))
print(pd.Series([5, 4, 3, 2, 1]))

# The benefit of using a series is that we can specify the index that is used.
# For example:
series = pd.Series([5, 4, 3, 2, 1], index=['a', 'b', 'c', 'd', 'e'])
print(series)
# Note that if you are using a dictionary, the indices will automatically be the keys and the values will be the values of the dictionary.

# We can also index the series in several ways

# 1. Numerical Index
print(series[1]) # <- 4

# 2. Assigned Index
print(series['c']) # <- 3

# 3. Dot Indexing (Note that this only works when there are no spaces in the index)
print(series.b) # <- 4

# We can also apply our slicing syntax to them
print(series[:3]) # <- Prints first four items

# You can also try to apply all of the numpy methods of getting certain data such as passing a boolean to index certain parts of the series




##### DATAFRAMES #####

# A DataFrame is a sequence of series.
# It is like a two dimensional array or spreadsheet.

# We can create a DataFrame by passing in a dictionary of 1D iterables, a 2D ndarray, a Series, or another DataFrame
# For example

d = {
    "one" : pd.Series([4, 2, 6], index=['a', 'b', 'c']),
    "two" : pd.Series([1, 5, 3, 2, 6], index=['a', 'b', 'c', 'd', 'e'])
}

df = pd.DataFrame(d)
print(df)

# We can also specify the index and columns that we want to get specifically here
df = pd.DataFrame(d, index=['d', 'b', 'a'], columns=['two', 'three'])
print(df)





##### READING FILES #####

# While pandas allows you to read from a variety of different data files, one of the most common and efficient types is a csv file.
# CSV stands for comma separated values. This is because the file literally consists of values separated by commas.
# When a csv or other data file is read into pandas, it will be read as a DataFrame.

# Here we will import the diabetes.csv file.
# This file consists of different numerical data about diabetic patients.
diabetes_df = pd.read_csv("diabetes.csv")

# Some helpful parameters to use with this function are:
# sep: sep is the string that will be used to separate data, by default it is a ","
# skiprows: skips however many rows from the top as specified, used when the top of the data has extra information
# skipfooter: same as skiprows, but from the bottom

# We can get some info about the DataFrame using the info function.
diabetes_df.info()

# Just like in numpy, we can get a tuple of the shape of the DataFrame
print(diabetes_df.shape)

# We can use the head function to see the first 10 rows.
# You can pass in an argument to specify how many rows to show.
print(diabetes_df.head())
# Similarly you can also use the tail function to get the last 10 rows.
print(diabetes_df.tail())





##### BASIC OPERATIONS #####

# These are some basic functions you will use to perform basic operations.

### Value Counts ###

# The value_counts function allows you to see how many times each data point occurs.
# For example, if we want to find out how many of the patients were and weren't diabetic, we could run the following.
print(diabetes_df.Diabetic.value_counts())

# If we wanted to see the frequency of the ages of the patients, but in ascending order, we could run the following line.
print(diabetes_df.Age.value_counts(ascending=True))

### Sorting ###

# The sort_values function allows you to sort the values along either axis.
# You can specify whether you want ascending order (ascending by default), the kind of algorithm (quicksort by default), and the positions of the NaN values (last by default) to name a few.

# While our data here is only numerical, remember that you can also use this function on strings.
# Here we will sort the data by the BMI in descending order.
print(diabetes_df.BMI.sort_values(ascending=False))

# This returned a series of the sorted BMIs with the originial index.
# While this kind of output may be helpful for some applications, to see some correlations, you will likely want to sort the whole DataFrame by one or multiple parameters.
# To do this, we can use the by attribute.

# Here we can sort the DataFrame by BMI in ascending order 
print(diabetes_df.sort_values(ascending=False, by="BMI"))


# We can also specify multiple features to sort the data by. For example, if we wanted to sort by age, and then sort by BMI within each age group, we could run the following.
print(diabetes_df.sort_values(by=["Age", "BMI"]))

### Boolean Indexing ###

# We can also create a series of booleans by running a conditional operator on a series like below.
print(diabetes_df.Diabetic == 1)

# But we can also index a DataFrame with this new series.
# For example, if we only wanted to see diabetic patients:
print(diabetes_df[diabetes_df.Diabetic == 1])

# We can also use conditional operators, but we use & for and, | for or, and ~ for not instead of the words like in python.
# For example:
print(diabetes_df[(diabetes_df.Diabetic == 1) & (diabetes_df.Age < 30)])

### String Handling ###

# For this example, let's import the second dataset that we have.
doctors_df = pd.read_csv("doctors.csv")

# This dataset contains the names of doctors and their patients' IDs
print(doctors_df.head())

# Now, using the previous boolean indexing syntax along with built in string functions, we can search for a doctor.
# Let's say that we only know that their first name is Billie, then we can search for strings that contain the string, "Billie" and find the doctor we are looking for.
print(doctors_df[doctors_df.Physician.str.contains("Billie")])





##### INDEXING #####

# With pandas, we can index rows and columns of data by using labels.

### Set Index ###

# Using the set_index function, we can set one or more columns as the index of a DataFrame.
# By default the index is just a number starting from 0 to one less than the number of rows, as you can see when we run the head function.
print(diabetes_df.head())

# However, if we use the set_index function, then we will see that the index will now be whatever column we set it to be.
# Let's set the index to the patient ID.
print(diabetes_df.set_index("PatientID"))

# These changes have not been saved to the diabetes_df functions however.
print(diabetes_df.head())

# If we want to do that, we must use the inplace parameter.
diabetes_df.set_index("PatientID", inplace=True)
print(diabetes_df.head())
# Note that this is the case for almost all of the functions we have covered.

# We can also reset the index like so:
diabetes_df.reset_index(inplace=True)
print(diabetes_df.head())

### Sort Index ###

# The sort index function allows us to sort the DataFrame by the index.

# First let's set the index to the patient ID again.
diabetes_df.set_index("PatientID", inplace=True)

# Next we can sort and print.
diabetes_df.sort_index(inplace=True)
print(diabetes_df.head())

diabetes_df.reset_index(inplace=True)

### Loc and iLoc ###

# Loc will return all of the rows with the specified index

# First let's set the index to the age
diabetes_df.set_index("Age", inplace=True)

# Next let's use the loc function to find all people are are 21
print(diabetes_df.loc[21])

# We can also do the same thing even if age is not the index
diabetes_df.reset_index(inplace=True)
print(diabetes_df.loc[diabetes_df.Age == 21])

# iLoc is mainly based on integer positioning.
# This is favorable because it allows for slicing.

# If we want to access the 537th patient, we can do this:
print(diabetes_df.iloc[537])

# We can also access a list of individuals.
print(diabetes_df.iloc[[537, 1002, 1536]])

# We can also slice to get the second to fifth patients
print(diabetes_df.iloc[1:5])






##### GROUPBY #####

# The groupby function allows you to group a DataFrame based on certain criteria, 
# apply a function to each group, and combine the results in a new DataFrame.

# Let's group the data by the age.
# This will return a groupby object, but to visualize this, we can cast it to a list.
print(list(diabetes_df.groupby("Age")))

# We can also iterate through the groups.
for group_key, group_value in diabetes_df.groupby('Age'):
    print(group_key)
    print(group_value)
    # try commenting one of these lines out then run it to see the individual keys and values

### Groupby Aggregation ###

# We can get something similar to value_counts by using groupby chained to the size function.
print(diabetes_df.groupby("Age").size())

# We can use the agg function, for aggregate to perform calculations on the groups, for example:
print(diabetes_df.groupby("Age").agg("min"))
# That will return the minimum value for each feature of each group.

# We can also aggregate with multiple functions.
print(diabetes_df.groupby("Age").agg(["min", "max"]))
# This will give us the minimum and maximum values of each feature for each age group.

# We can also group by multiple features.
print(list(diabetes_df.groupby(["Age", "Diabetic"])))
# This will first group the data into age groups, then into subgroups of diabetic and nondiabetic

# Then, we can aggregate this data as well
print(diabetes_df.groupby(["Age", "Diabetic"]).agg(['max', 'min']))

# We can also aggregate in a dictionary like manner to run specific functions on specific features of the groups.
print(diabetes_df.groupby(["Age", "Diabetic"]).agg({"DiabetesPedigree": ['max', 'min']}))





##### RESHAPING #####

# There are several ways to reshape your data

# The melt function allows you to turn columns into rows.
print(diabetes_df.melt())

# You can also specify the columns to use as identifier variables and those to unpivot
print(diabetes_df.melt(id_vars=['PatientID'], value_vars=['Diabetic', 'Age']))

# You can also do the opposite which is called pivoting
print(diabetes_df.pivot(columns='Age', values='Diabetic'))
# Although pivoting is not very useful in this case

# We can also concatenate data together.
# If the DataFrames both have the same columns, the rows will just be added, otherwise, if the index is the same, then the columns will be added.
# First let's set the index to the PatientID for the diabetes DataFrame and the doctors DataFrame.
diabetes_df.set_index('PatientID', inplace=True)
doctors_df.set_index('PatientID', inplace=True)

# Let's concatenate the data together now:
print(pd.concat([diabetes_df, doctors_df]))

diabetes_df.reset_index(inplace=True)

# One last common way to reshape the data is to pop columns.
# If you are training a machine learning model, you want to use only the relevant data, so you may have to remove some features, here is how to do that.

# If we are trying to train a model to recognize diabetes in a patient, we may not need the following data, so we will remove it:
diabetes_df.pop("PatientID")
diabetes_df.pop("Pregnancies")

# We also don't want to train our model when it knows the results, but we will still need that data to score the model, so we will store it in a series
diabetic = diabetes_df.pop("Diabetic")
print(diabetic)

