# Python Crash Course

This is where the lessons and challenges for the python crash course will be posted.

This course covers:
1. Basic Python
    * Datatypes and Variables
        * Numerical
        * Strings
        * Printing
        * Lists
        * Dictionaries
        * Tuples
        * Sets
        * Booleans
    * Comparison Operators
        * If
        * Else
        * Elif
    * Loops
        * While Loops
        * For loops
        * range() Function
        * List Comprehension
    * Functions
        * Function Declaration
        * Built in Functions
        * Map and Filter
        * Lambda Expressions
2. Numpy
    * NDarrays
        * Declaration
        * Random
        * Attributes and Methods
        * Indexing and Selection
        * Operations and Arithmetics
3. Pandas
    * Series and Dataframes
        * CSV and Declaration
        * Indexing
    * Operations
        * Grouping
        * Merging
        * Concatenation
        * etc.
4. Matplotlib (Pyplot)
    * Basic Plotting
        * Labeling
        * Colors
        * Plot Types
        * Line Types
        * Ranging
        * Custom Scale and Ticks
    * Multiplots
    * Subplots

[Our Club Website](https://www.townviewai.com/) | [Our Cloud Computing Service](http://www.townviewai.com:4200/) |  [Our Discord Server](https://discord.gg/MTChRGn) | [Our YouTube Channel](https://www.youtube.com/channel/UCtoopcfdAcxFliCMCVt93qA) | [Our Twitch Channel](https://www.twitch.tv/townviewml) | [Our Instagram](https://www.instagram.com/townviewml/)
