##### INTRODUCTION #####

# Sometimes, you may need to run the 
# same segment of code multiple times.
# Instead of typing the same block of 
# code over and over again, we can use
# functions.





##### WRITING FUNCTIONS #####

# The following example shows how to write a function that prints "Hello World"
def hello_world():
    print("Hello World")

# This is how you call, or use, that function
hello_world() # <- Hello World

# We can also add arguements to the function. Arguements allow you to pass values
# to the function.
def print_name(name):
    print("Hello ", name)

# Here is how you call a function with arguments
print_name("Jimmy") # <- Hello Jimmy

# We can also have functions return values. For example, if we wanted a method 
# to calculate the hypotenuse of a triangle with the Pythagorean theorem like below.

import math # import built-in math functions for Pythagorean theorem

def pythagorean_theorem(a, b):
    return math.sqrt(a**2 + b**2) # math.sqrt is a function of the math class that takes in a numerical argument and returns the squeare root

# this is how to use the method
a = 3
b = 4
c = pythagorean_theorem(a, b)
print(c) # <- 5

# You can also give default values as arguments in functions, for example
def area(radius, pi=3.14):
    return pi * radius ** 2

# We can call the function in three ways
print(area(2)) # <- 12.56
print(area(2, 3.1415)) # <- 12.566
print(area(2, pi=3.1415)) # <- 12.566





##### BUILT-IN FUNCTIONS #####

# Python has some functions that are already built in
# that allow you to do things without having to write 
# extra code.

### Documentation Functions ###

# Sometimes, you may forget the name of a function
# or how to use it, or maybe you are just learning
# about a new set of functions, but in these cases,
# we can use the dir and help functions.

# The example below will display all of the functions and attributes of a string
dir("test")

# If you wanted to learn more about a particular function, such as title(), you could do this:
help("test".title)

# One more function that might come in handy is the type() function.
# If you don't know the type of some data, then you can run the type() function to find out.
print(type("test")) # <- str

### Math Functions ###

# Out side of the math library that was imported earlier,
# Python comes with some functions that are already built-in.

# abs() - returns absolute value
print(abs(-5)) # <- 5

# round() - rounds numbers
print(round(6.6)) # <- 7

### Iterable Functions ###

# This next set of functions will help to parse iterable datatypes.

# len() - tells you the length of an iterable
a = [1, 2, 3, 4, 5, 6, 7, 9, 10]
print(len(a)) # <- 10

# sum() - returns the sum of an iterable
print(sum(a)) # <- 55

# max() and min() - return the greatest and least values of the iterable
print(max(a)) # <- 10
print(min(a)) # <- 1

# reversed() - returns a reverseiterator object which is like a backwards version of the iterable
print(list(reversed(a))) # <- [10, 9, 8, 7, 6, 5, 4, 3, 2, 1]
# note that we still had to convert the output to a list in order to get a printable value

# enumerate() - returns a list of tuples where the first value of each tuple is the index and the second value is the value in the iterable.
print(list(enumerate(a))) # <- [(0, 1),
                          #     (1, 2),
                          #     (2, 3),
                          #     (3, 4),
                          #     (4, 5),
                          #     (5, 6),
                          #     (6, 7),
                          #     (7, 8),
                          #     (8, 9),
                          #     (9, 10)]

# zip() - puts the values of two iterables together in a list of tuples
b = list(reversed(a))
print(list(zip(a, b))) # <- [(1, 10),
                       #     (2, 9),
                       #     (3, 8),
                       #     (4, 7),
                       #     (5, 6),
                       #     (6, 5),
                       #     (7, 4),
                       #     (8, 3),
                       #     (9, 2),
                       #     (10, 1)]

# map() - applies a function to every single element in an iterable
def square(x):
    return x**2
print(list(map(square, a))) # <- [1, 4, 9, 16, 25, 36, 49, 64, 81, 100]

# filter() - returns an iterable only containing the values that evaluate to True of the function you give.
def even(x):
    return x % 2 == 0
print(list(filter(even, a))) # <- [2, 4, 6, 8, 10]

### Files ###

# We can also read and write files in Python using the open() function.

# The following example opens a file called test.txt and writes, "Hello World" in it.
test_file = open("test.txt", "w") # this will open the file test.txt in write mode
test_file.write("Hello World")
test_file.close() # it is important to always close a file once you are done with it

# You will notice that the second arguement, a 'w', was to indicate write mode, but there are also other modes.
# w and r are for write and read, but you can also add the following flags as well:
# x - create a new file, but it will not work if there is already a file
# a - open the existing file and only append to the end of the file

# Here is an example of how to read that file, but this time, we will use the with keyword which will
# close the file on its own and only open it for a specific block.
with open("test.txt", "r") as f:
    print(f.read())

### Other Functions ###

# Here are some other functions you might find useful.

# del - del is not really a function, but it just deletes data from memory, so for example, if we wanted to delete list b from earlier, we would do this:
del b

# eval() - eval allows you to run the python interpreter on a string
print(eval("5+7%2")) # <- 6

# exec() - exec is the same as eval, except the code actually runs in this instance
exec("num = 50")
print(num) # <- 50

# The last function is format(). The format function is the latest way to format a string in Python, but older methods include
# f strings and using %.
# Here are some examples that all do the same thing using the different methods
name = "Jimmy"
age = 15

print("{} is {} years old".format(name, age)) # <- Jimmy is 15 years old
print(f"{name} is {age} years old") # <- Jimmy is 15 years old
print("%s is %d years old" % (name , age)) # <- Jimmy is 15 years old





##### LAMBDA EXPRESSIONS #####

# Sometimes, for a simple map or filter call, it can be annoying to write an entire function just for the one function call.
# Instead, we can just use a lambda expression to write functions in one line.

# For example, if we wanted to write a squaring function, we could do this
square = lambda x : x**2

# as you can see, the syntax is lambda attributes : return expression

# Here is one to add two numbers
add = lambda a, b : a + b

# This is a function to check if a number is even
even = lambda x : x % 2 == 0

