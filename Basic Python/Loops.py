##### INTRODUCTION #####

# Sometimes we may want to perform a task multiple times over,
# but it can be tedious to write the same code over and over again.
# So, in these cases we can use loops to allow python to replicate
# the task on its own.





##### WHILE LOOPS #####

# Just like most python syntax, while loops 
# do exactly what they sound like they would do.
# Just like an if statement, you input a condition,
# and your code will only run if the condition is
# true. But, this time instead of moving on, in a 
# while loop, the code keeps on running as long as 
# the condition stays true. So, while the condition
# is true, the code will run.

# Here is an example of a loop that will run 5 times and print the values 0-4
i = 0 # This is the index variable. It will be the variable relevant to the condition
while i < 5: # This condition means that as long as i is less than five, the indented code that follows should be run.
    print(i) # Here we will print the index
    i += 1 # This is a shorthand for i = i + 1, but it will ensure that the loop won't get stuck forever.
           # If this statement isn't added, then i will always equal 0 and the loop would never end because
           # i would never be >= 5.

# Sometimes, you may want the loop to run forever. For example, if
# you are trying to get a value rolled on a die *number cube*, you may
# need to restrict the input range to be between 1 and 6 inclusive and
# ask the user to try again if the input is out of that range.

# This will be an infinite loop that will run forever if not broken out of.
while True:
    die_value = int(input("What did you roll? "))
    if 1 <= die_value <= 6:
        break # this keyword will end any loop that you are in

# We can also write a simple guessing game like this:
guess = 0
while guess != 3:
    guess = int(input("Guess a number between 1 and 10: "))
print("You guessed the correct number!")





##### FOR LOOPS AND RANGE FUNCTION #####

# Sometimes, rather than having your loop be 
# conditional, it might make more sense to have
# your loop be iterative or run a specific number
# of times. This is where for loops come in.

# First we are going to create a list of integers, then we will print every value in the list
nums = [4, 5, 2, 7, 4, 8, 9, 3, 6]

for num in nums: # what this will do is create a variable called num that store the value of an element of nums
    print(num) # now we can just print the element

# Sometimes, instead of iterating through iterable datatypes, we may want to run some code a specific number of times.
# For this, we can use the range function. The range function is something called a generator, meaning that it 
# generates different values for iterations in a loop.

# if we want a for loop that runs 5 times, and through the values 0 to 4, we would use the range function like this
for x in range(5):
    print(x)

# there are three values you can feed into the range function, as shown below:
# range(int: start = 0, int: end, int: step = 1)

# The start value is by default set to 0, and the step value is 1, so the only one you must declare to use the range function is end.
# The way that it works is that it will start with the start value, and keep incrementing by step amount until the value is >= end.

# for example, if you wanted to print all the odd numbers between  5 and 21 inclusive, you might do
for x in range(5, 22, 2):
    print(x)





##### LIST COMPREHENSION #####

# We saw how to make lists, but we may also want to create them 
# automatically.

# For example, if we wanted a list called a with all multiples of 2 from 0 to 20 inclusive,
# we might do this.
a = []
for x in range(0, 21, 2):
    a.append(x) # the append function appends items to a list
print(a)

# While the previous code block works, it is not a very elegant solution.
# This is where list comprehension comes in. For example, to do the same
# thing, we can just write:
a = [x for x in range(0, 21, 2)]
# and it will still result in the same thing
print(a)

# We can also make this a bit more complicated.
# Say for example if we wanted to make a list b
# with all of the values in a, but only the 
# ones that are not multiples of 3, we could do
# this:
b = [x for x in a if x % 3 != 0]
print(b)

# The above line is the same as
# b = []
# for x in a:
#     if x % 3 != 0:
#         b.append(x)
# print(b)

# We can also add things to the comprehension.
# If we want to create a list of strings that
# has whatever corresponding value from a 
# number of cakes as the elements, then we might
# do this:
# (Hint: The str function turns our integer x into a string of the corresponding value)
cakes = [str(x) + " cakes" for x in a]
print(cakes)




##### NESTED LOOPS #####

# Sometimes, we need loops inside of other loops.
# This concept is know as a nested loop.

# This code will print a 5 by 5 square of *s
for x in range(5):
    for y in range(5):
        print('*', end='')  # end will specify the ending character of a print, so this will print on the same line
    print()

# this could also be done with something like:
# for x in range(5):
#   print("*" * 5)
# with greater efficiency, but this is just for
# the sake of example.

# A more useful scenario to use nested loops is with multiple dimensional arrays,
# or in other words, arrays inside of arrays.

# Here is a 2D array:
a = [[1, 2, 3],
     [4, 5, 6],
     [7, 8, 9]]

# We can index elements by first indexing the row, or the array in the outer array,
# then we again index the inner array to access the element.
# For example, if we want the 8, we can see that it is the third row and second column.
# Translating that into indices, we get that it is at row 2 and column 1, so it would be
# accessed as follows:
print(a[2][1]) # <- 8

# If we want to print all of the elements of the array, we could do it using a nested loop as follows:
for row in a:
    for col in row:
        print(col, end=' ')
    print()

