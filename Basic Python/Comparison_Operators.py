##### INTRODUCTION #####

# In this lesson, you will learn how
# to use comparison operators in python.
# Comparison operators revolve around the 
# boolan datatype. You will be able to 
# tell python to determine which code to run
# and which code not to run by itself.





##### BOOLEANS CONTINUED #####

# Before we go into the more advanced stuff, we
# will need to understand how booleans work further
# in depth.

### Boolean Evaluation ###

# Booleans expressions can be evaluated just like
# math expressions. But just like math expressions,
# there are some operations you must understand first.

# Here is a table of the boolean operations and how they function

# Operator | Description
# -----------------------------------------------------------------------
#   not    | negates input
#   and    | only true if both inputs are true
#   or     | true if either input is true
#   xor    | true only when one input is true, but not if both or neither

# Examples

# Not
the_earth_is_flat = not True # <- False
the_earth_is_a_spheroid = not the_earth_is_flat # <- True

# And
has_license = True
has_car = False

can_drive = has_license and has_car # <- False

# Or
studied = False
cheated = True

passed_test = studied or cheated # <- True

# Xor
get_cookies = True
get_ice_cream = True

get_what_you_want = get_cookies ^ get_ice_cream # <- False

#    a   |   b   | a and b | a or b | a xor b | not a
# ----------------------------------------------------
#  true  | true  | true    | true   | false   | false
#  true  | false | false   | true   | true    | false
#  false | true  | false   | true   | true    | true
#  false | false | false   | false  | false   | true

# We can also chain them together and come up with more complex expressions
# However, for these kinds of expression, you must remember the order of operations
# 1. ()
# 2. xor
# 3. not
# 4. and
# 5. or

a = (True and False) or not False or True # <- True
# =     (False) or not False or True
# =         False or True or True
# =              True or True
# =                  True

print(a) # <- True

### Converting Datatypes to Booleans ###

# We can also apply operations to other
# datatypes to get a boolean.

# There are a couple of different operators to do this

# Operator | Description
# ------------------------------------------------------
#   ==     | True if both values are equal
#   !=     | True if both values aren't equal
#   >      | greater than (only for numbers)
#   <      | less than (only for numbers)
#   >=     | greater than or equal to (only for numbers)
#   <=     | less than or equal to (only for numbers)

# Examples
age = 15

is_15 = age == 15 # <- True
is_not_15 = age != 15 # <- False
could_be_teen = age > 12 # <- True
cannot_drink = age < 21 # <- True
is_alive = age >= 0 # <- True
cannot_vote = age <= 17 # <- True

# we can also chain them together like this (this is a feature specific to python)
is_teen = 13 <= age <= 19 # <- True
# in most other languages you would have to write something like this
is_teen = age >= 13 and age <= 19





##### IF STATEMENTS #####

# In order to use the boolean to make 
# python run specific lines of code, you
# must use if statements.

# An if statement must have a boolean as the condition
# and code to be run if the condition is true.

# Here is a simple if statement that will only print
# that the user is a teen, if the user is a teen.
if is_teen:
    print("You are a teen.")
# any code that you want to be run if the condition
# is true must be indented. Any code that is not indented
# will not be counted as part of the if statement.

# Here is another example
if cannot_vote and cannot_drink:
    print("You are not an adult.")

### Else ###

# Elses are extensions of if statements
# that do exactly what they sound like:
# they contain the code that is run if 
# the condition is false.

# The indentation rules still apply here as well
if is_teen:
    print("You are a teen.")
else:
    print("You are not a teen.")
# as you can see, if is_teen is true, then 
# "You are a teen." will be printed, otherwise
# "You are not a teen." will be printed.

### Elif ###

# Sometimes, rather than just true
# or false, you might have more complicated
# decisions to make, for this you will need to 
# use an else if statement, or, in python, elif.

# Here is an example
if cannot_vote and cannot_drink:
    print("You are not an adult")
elif cannot_drink and not cannot_vote:
    print("You are between 18 and 20")
else:
    print("You are an adult")

