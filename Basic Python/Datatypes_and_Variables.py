##### INTRODUCTION #####

# In this lesson, you will learn about
# the basic datatypes in python and some 
# of the basic things you can do with them.





##### NUMERICAL DATATYPES #####

### Ints ###

# Ints, or integers, are the most common 
# numerical datatypes in python. Just as the
# name suggests, they store integers.

# this line creates an integer variable called a with a value of 5
a = 5

# this line creates an integer variable called b that is 5 greater than a
b = a + 5

### Floats ###

# Floats hold decimal values. If you need
# more precision than an int, then you use a
# float.

# this line creates a float called pi with the first 5 digits of pi
pi = 3.1415

# this line creates a float called tau which is two times the value of pi
tau = 2 * pi

### Basic Numerical Operations ###

# These are the four basic operations in python and their symbols:
# Addition: +
# Substraction: -
# Multiplication: *
# Division: /

# Here are some examples:
s = 7 + 3 # <- 10
d = 200 - 43 # <- 157
p = 3 * 18 # <- 54
q = 18 / 3 # <- 6

### Other Operations ###

# Although you can do a lot with those 
# four operations, sometimes, especially 
# in machine learning, it can be easier 
# to have some shortcuts.

# These are three more operations that can help to write 
# cleaner code: 
# Exponent: **
# Integer Division: //
# Remainder: %

# Here are some examples:
e = 4 ** 3 # <- 64
intd = 18 // 4 # <- 4
rem = 26 % 4 # <- 2




##### STRINGS #####

### Basic Declaration ###
# Strings are basically the datatypes that hold
# text. Any time you need to store textual data 
# or messages, you should rely on strings. Strings
# have many powerful built in functions in python,
# but for now we will stick with the basics.

# this line creates a string called message with the message "Hello World!"
message = "Hello World!"

# this line creates a string called message1 which also store the message "Hello World!"
message1 = 'Hello World!'

# As you can see, both single and double quotes make strings.
# The biggest reason to use one or the other is if you have 
# quotes inside of your string.

# In this case, you may want to use single quotes:
quote = 'And then I said, "No you!"'

# But in this case, you may want to use double quotes:
quote1 = "And then I said, 'No you!'"

### Escape Characters ###

# Sometimes, you may want to use both kinds of quotes
# in your string. In that case, you will need to use
# an escape sequence. Escape sequences consist of a
# backslash followed by a character.

# For example, if you wanted to use both kinds of quotes, you might do this:
quote2 = "And then I told her, \"He said, 'No You!'\""
# or
quote2 = 'And then I told her, "He said, \'No You!\'"'

# In both cases, the backslash will not be counted 
# as part of the string, instead, you will just see
# the quote.

# But those aren't the only kinds of escape characters.
# These are some of the most common ones in addition to the quotes:
# Single and Double Quotes: \', \"
# Backslash: \\
# New Line: \n
# Tab: \t

### Basic Operations ###

# There are some basic operations you can apply
# to strings similar to the numerical datatypes.

# This is how you add strings together
first_name = "Jimmy"
last_name = "Abbondandolo"
full_name = first_name + last_name # <- JimmyAbbondandolo
# for better formatting, we could also add a space
full_name = first_name + " " + last_name # <- Jimmy Abbondandolo

# Strings can also be multiplied as shown below
mult_string = "test" * 3 # <- testtesttest

### Printing ###

# Printing allows you to output data to the user.

# This is how to print "Hello World!"
print("Hello World!") # <- Hello World!
# or we can use the variable from earlier
print(message) # <- Hello World!

# We can also print numerical data
print(5) # <- 5

# We can also use commas to seperate the data with spaces
age = 1928283.293411
print(full_name, age) # <- Jimmy Abbondandolo 1928283.293411





##### BOOLEANS #####

# Booleans are the simplest data types in python.
# They simply store True or False, but they can be
# very important.

# This line creates a boolean called can_drive and sets it to false (I don't trust you behind the wheel):
can_drive = False

# This line sets can_drive to True (oh no!! we are dead):
can_drive = not can_drive # <- True

# We can also print booleans like this
print(can_drive) # <- True

# Using the remainder operator, we can also find out if a number is divisible by another number.
fifteen_is_divisible_by_3 = 15 % 3 == 0 # <- True





##### ITERABLES #####

### Lists ###

# Lists are just as they sound, they
# store lists of information. With lists,
# you can index and manipulate them at any time.

# this line creates a list of ints called nums with the numbers 1 through 5
nums = [1, 2, 3, 4, 5]

# this line prints out the list
print(nums) # <- [1, 2, 3, 4, 5]

# this line prints the first element in the list
print(nums[0]) # <- 1
# this is called indexing

# lists are numbered starting from 0 rather than 1, so to print the 3rd element, or the the three, you would do this:
print(nums[2])

# to change an element, say for example replacing the 3 in the list (at index 4) for a 6, you would do this:
nums[2] = 6
# now if we print nums, we get:
print(nums) # <- [1, 2, 6, 4, 5]

### Tuples ###

# Tuples are similar to lists, but they 
# cannot be changed.

# this line creates a tuple called coordinates with the values 2, 7, and 5
coordinates = (2, 7, 5)

# while we cannot change these values anymore, we can still index them
print("The x coordinate is", coordinates[0]) # <- The x coordinate is 2

### Sets ###

# Sets can be modified using some functions that we will
# discuss later, but they cannot be indexed. They also will
# only contain on element of each kind of value

# This line creates a set called set1 with 1, 2, and 3
set1 = {1, 2, 3}
print(set1) # <- {1, 2, 3}

# Watch what happens when we create a set with a repeating value
set2 = {1, 2, 3, 3}
print(set2) # <- {1, 2, 3}

### Dictionaries ###

# Dictionaries are one of the most powerful iterables. Dictionaries
# allow you to store values under keys. The keys are what you index with, 
# and the values are what is returned upon indexing. The key and value can
# be of any datatype, even another dictionary.

# This line creates a dictionary called grades
grades = {'Jimmy': 0.9, 'Jill': 0.95, 'Timmy': 0.78, 'Till?': 0.99}
# in this example, the names are the keys, and the grades are the values
print(grades) # <- {'Jimmy': 0.9, 'Jill': 0.95, 'Timmy': 0.78, 'Till?': 0.99}

# we can "index" elements of the dictionary like this
# in this case we will get Timmy's grade
print(grades["Timmy"]) # <- 0.78

# We can also change the values in this way
# in this case, we will increase Till's grade by .01
grades["Till?"] = grades["Till?"] + 0.01 # TIP: a faster way to do this would be:
                                         # grades["Till?"] += 0.01

print(grades) # <- {'Jimmy': 0.9, 'Jill': 0.95, 'Timmy': 0.78, 'Till?': 1}

##### TAKING INPUT #####

# Sometimes, you will need to take input from the user
# in your programs. Python makes this very simple with
# the input function. Normally, you might ask your user
# to input a string or numerical datatype. You could
# possible take in other things like lists, but that 
# would be difficult for the user.

# this line will ask the user for their name, accept the name, store  it in a variable, and then we will print it out.
name = input("What is your name? ")
print(name)

# For age, it is a little different. The input function only outputs strings, so you will have to convert it to an int.
# Every datatype has its own conversion function, for integers, it is called int
age = int(input("How old are you? ")) # As you can see, whatever the user inputs will go into the int function and get converted to an int, then it will be stored in age.

# Now we can use it like a normal int
print("In five years, you will be: ", age + 5)

